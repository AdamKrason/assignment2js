import { configureStore } from '@reduxjs/toolkit';
import translationReducer from './TranslationSlice';

export default configureStore({
	reducer: {
		translation: translationReducer,
	},
});
