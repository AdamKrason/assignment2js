import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

const apiKey = 'allenteforever';
const apiUrl = 'https://snapdragon-reliable-badge.glitch.me/translations';

export const getUserAsync = createAsyncThunk(
    'translation/getUsersAsync',
    async (payload) => {
        const resp = await fetch(`${apiUrl}?username=${payload}`);
        if (resp.ok) {
            const user = await resp.json();
            return {user};
        }
    }
)

export const addUserAsync = createAsyncThunk (
    'translation/addUserAsync',
    async (payload) => {
        const resp = await fetch(apiUrl, {
            method: 'POST',
            headers: {
                'X-API-Key': apiKey,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: payload.username,
                translations: []
            }),
            });
            if (resp.ok) {
                const user = await resp.json();
                return { user };
        }
    }
);

export const toggleCompleteAsync = createAsyncThunk(
	'translation/completeTranslationAsync',
	async (payload) => {
		const resp = await fetch(`${apiUrl}?username=${payload.username}`, {
			method: 'PATCH',
			headers: {
                'X-API-Key': apiKey,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ 
                completed: payload.completed
            }),
		});

		if (resp.ok) {
			const user = await resp.json();
			return { user };
		}
	}
);

export const deleteUserAsync = createAsyncThunk(
	'translation/deleteUserAsync',
	async (payload) => {
		const resp = await fetch(`${apiUrl}/${payload.id}`, {
			method: 'DELETE',
		});

		if (resp.ok) {
			return { 
                id: payload.id 
            };
		}
	}
);

export const translationSlice = createSlice({
    name: 'translations',
    initialState: [],
    reducers: {
        addUser: (state, action) => {
            const user = { 
                username: action.payload.username,
                translations: [],
                completed: false
            };
            state.push(user);     
        },
        toggleComplete: (state, action) => {
             state[index].completed = action.payload.completed;
        }
    },
    extraReducers: {
        [getUserAsync.fulfilled]: (state, action) => {
            return action.payload.users;
        },
        [addUserAsync.fulfilled]: (state, action) => {
            state.push(action.payload.user);
        },
        [toggleCompleteAsync.fulfilled]: (state, action) => {
			const index = state.findIndex(
				(user) => user.username === action.payload.user.username
			);
			state[index].completed = action.payload.user.completed;
		},
    },
});
export const { addUser, toggleComplete } = translationSlice.actions;
export default translationSlice.reducer;