import images from "./imageImporter"
import "../../css/Translation.css";


function TranslationBox(props) {
    let str = props.tString;
    let list = [];
    for (let i = 0; i < str.length; i++){
        for (let j = 0; j < Object.keys(images).length; j++){
            if (Object.keys(images)[j] === str.charAt(i)) {
                list.push(Object.values(images)[j])
                break;
            }
        }
    }

    return (
            list.map((item, i) => {
                return (
                    <div key={i}>
                        <img src={item}></img>
                    </div>
                )
            }
    ))  
}

export default TranslationBox;