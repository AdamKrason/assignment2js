import "../../css/Translation.css";
import { useState } from "react";
import TranslationBox from "./TranslationBox";
import authGuard from "../AuthGuard";
import { useDispatch, useSelector } from "react-redux";
import { addTranslationToUser, loggedIn, setTranslation, fetchUserByName } from "../../state/TranslationSlice";


function TranslationPage() {
    const [tString, setTString] = useState("");
    const [propString, setPropString] = useState("");

    const user = useSelector(state => state.translation);
    const dispatch = useDispatch();

    function handleChange(event) {
        setTString(event.target.value);
    }

    function handleTranslate() {
        setPropString(tString.toLowerCase());

        let tempArr = [];
        user.translations.map((item) => {
            tempArr.push(item);
        });

        tempArr.push(tString);

        dispatch(addTranslationToUser({id: user.id, translation: tempArr}));
    }

    return (
        <div className="translationMain">
            <h1 id="translateWords">Translate your words!</h1>
            <div id="inputDiv">
                <input id="tInput"
                    type="text"
                    placeholder="Enter the string to translate"
                    onChange={handleChange}
                />
                <button id="translateBtn" onClick={handleTranslate}>Translate</button>
            </div>
            <div className="translationImages">

                <TranslationBox tString={propString}></TranslationBox>
            </div>
        </div>
    )
}

export default authGuard(TranslationPage);