import a from "../../data/resources/individial_signs/a.png"
import b from "../../data/resources/individial_signs/b.png"
import c from "../../data/resources/individial_signs/c.png"
import d from "../../data/resources/individial_signs/d.png"
import e from "../../data/resources/individial_signs/e.png"
import f from "../../data/resources/individial_signs/f.png"
import g from "../../data/resources/individial_signs/g.png"
import h from "../../data/resources/individial_signs/h.png"
import i from "../../data/resources/individial_signs/i.png"
import j from "../../data/resources/individial_signs/j.png"
import k from "../../data/resources/individial_signs/k.png"
import l from "../../data/resources/individial_signs/l.png"
import m from "../../data/resources/individial_signs/m.png"
import n from "../../data/resources/individial_signs/n.png"
import o from "../../data/resources/individial_signs/o.png"
import p from "../../data/resources/individial_signs/p.png"
import q from "../../data/resources/individial_signs/q.png"
import r from "../../data/resources/individial_signs/r.png"
import s from "../../data/resources/individial_signs/s.png"
import t from "../../data/resources/individial_signs/t.png"
import u from "../../data/resources/individial_signs/u.png"
import v from "../../data/resources/individial_signs/v.png"
import w from "../../data/resources/individial_signs/w.png"
import x from "../../data/resources/individial_signs/x.png"
import y from "../../data/resources/individial_signs/y.png"
import z from "../../data/resources/individial_signs/z.png"

const images = {
    a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z
}

export default images;