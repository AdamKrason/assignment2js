import { BrowserRouter, NavLink, Route, Routes } from "react-router-dom";

import ProfilePage from '../Profile/ProfilePage';
import Startup from "../Startup/Startup";

import logo from "../../data/resources/logo_img/Logo.png";
import MiniProfile from "../Navigation/MiniProfile";

import "../../css/TopNavbar.css";
import Translation from "../Translation/TranslationPage";

import { useSelector } from "react-redux";

//<NavLink to="/" ><a href="" className="logo"> <img className="logo" src={splash} alt=""></img></a></NavLink>
const TopNavbar = () => {
    const user = useSelector(state => state.translation);

    return (
        <BrowserRouter>
            {user.username != null ? (
                <div className="navigation">
                    <div className="container">
                        <NavLink to="/translation" className="element">
                            <div className="innerContainer">
                                <div className="alignLogo">
                                    <img className="logoPicture" src={logo} width="40" alt=""></img>
                                </div>
                                <p className="logo">Lost in Translation</p>
                            </div>
                        </NavLink>
                        <NavLink to="/profile" className="element"><MiniProfile /></NavLink>
                    </div>
                </div>
            ) : (
                <div id="navBeforeLogin" className="navigation">
                    <p className="logo">Lost in Translation</p>
                </div>
            )}
        <Routes>
            <Route path="/" element={<Startup />}/>
            <Route path="/profile" element={<ProfilePage />}/>
            <Route path="/translation" element={<Translation />}/>
        </Routes>
        </BrowserRouter>
      
    );
  };
  
  export default TopNavbar;

