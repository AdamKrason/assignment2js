import profilePic from "../../data/resources/profile_img/male-user.png";
import { useSelector } from 'react-redux';

import "../../css/MiniProfile.css";

const Profile = () => {
    const translations = useSelector((state) => state.translation);

    return(
        <div className="alignMiniProfile">
            <p className="fancyName">{translations.username}</p>
            <img src={profilePic} alt="Generic profile picture" className="fancyProfilePic"></img>
        </div>
    )
};

export default Profile;
