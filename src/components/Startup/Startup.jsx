import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "../../css/Startup.css";
import { fetchUserByName, loggedIn, deleteUser  } from "../../state/TranslationSlice";
import { useDispatch } from 'react-redux';
import logo from "../../data/resources/logo_img/Logo.png";

function LoginForm() {
    const [userName, setUserName] = useState("");
    const dispatch = useDispatch();
    const navigate = useNavigate();

    function handleLoginChange(event) {
        setUserName(event.target.value);
    }

    function handleLogin() {
        console.log('user entered: ' + userName);
		dispatch(fetchUserByName(userName)).unwrap().then(() => navigate("/translation"))

        // TODO: Remove code for mass deleting users posted in API
        /*
        for (let i = 2; i < 22; i++) {
            console.log(`Deleting user ${i}`);
            dispatch(deleteUser(i));
        }
        */
    }

    return (
        <div className="alignStartup">
            <div className="startupSplash">
                <div className="splashContainer">
                    <div className="startupAlignLogo">
                        <img className="logoPicture" src={logo} width="125" alt=""></img>
                    </div>
                    <div className="innerSplashContainer">
                        <p className="startupLogo">Lost in Translation</p>
                        <p className="startupElement">Get started</p>
                    </div>
                    
                </div>
            </div>
            <div className="outerLoginForm">
                <div className="loginForm">
                    <input
                        type="text"
                        placeholder="What's your name?"
                        onChange={handleLoginChange}
                        className="inputForm"
                    />
                    <button className="loginBtn" onClick={handleLogin}></button>
                </div>
            </div>
            
        </div>
    );
}

export default LoginForm;