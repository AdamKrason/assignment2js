import authGuard from "../AuthGuard";

const Translation = ( props ) => {
    if (props.state.length > 0) {
        return(
            props.state.map((item, i) => {
                return (
                    <div className="Profile" key={i}>
                        <p>{item}</p>
                    </div>
                )
            })
        )
    } else {
        return(
            <div className="Profile">
                <p id="records">No translations available</p>
            </div>
        )
    }
};

export default authGuard(Translation);
