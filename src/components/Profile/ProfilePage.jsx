import Translations from "./SavedTranslations";
import authGuard from "../AuthGuard";
import "../../css/ProfilePage.css";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { loggedOut, deleteTranslations  } from "../../state/TranslationSlice";



/*
https://snapdragon-reliable-badge.glitch.me/trivia
*/

const Profile = () => {
    const navigate = useNavigate();

    const user = useSelector((state) => state.translation);
    const dispatch = useDispatch();

    function handleLogOut() {
        navigate('/');
        dispatch(loggedOut())
    }
    
    function handleDelete() {
        console.log("Deleting records!")
        dispatch(deleteTranslations(user));
    }
    
    return(
        <div className="profile">
            <h1>{user.username}</h1>
            <button className="logOutBtn" onClick={handleLogOut}>Log out</button>
            <div className="profileContainer">
                <Translations className="translations" state={user.translations}/>
            </div>
            <button className="wipeBtn" onClick={handleDelete}>Wipe records</button>
        </div>
    )
};

export default authGuard(Profile);
