import { Navigate } from "react-router-dom";
import { useSelector } from "react-redux";

function authGuard(Component) {
  return function (props) {
    const user = useSelector(state => state.translation);
    let condition = user.username != null;

    if (condition) {
      return <Component {...props} />;
    } else {
      return <Navigate to="/" />;
    }
  };
  // how do we let people go here?
}

export default authGuard;