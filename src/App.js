import './App.css';
import TopNavbar from "./components/Navigation/TopNavbar";

function App() {
  return (
    <div className="App">
        <TopNavbar />
    </div>
  );
}

export default App;
