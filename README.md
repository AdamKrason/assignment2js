# lost-in-translation

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This repository contains the deliverables for assignment 2 in the frontend module at Noroff Accelerate. The assignment asks for an online sign language translator as a single page application using the React framwork.

We have freedom to be creative as we wish to, as long as it meets the minimum requirements described in appendix A.

(We decided to not be creative and copy the suggested styling because it already looks pretty)

#### IMPORTANT DISCLAIMER
Note that the first time a user attempts to authorize (log in), the API may use half a minute to respond. This is due to the fact that glitch has to reactivate the hosted API. Attempts to reauthorize when the API is online will be swift. (This will also be the case for translations.)

The single page application is currently hosted on Vercel:

https://assignment2js.vercel.app/

#### Link to component tree

[Figma](https://www.figma.com/file/eYoRzqo7tLMG6SD5FLayP3/SuperComponentTreeWithKraseAndKoie?node-id=0%3A1)

Can also be found in root-folder of the repository: "ComponentTree.pdf"

## Table of Contents

- [lost-in-translation](#lost-in-translation)
      - [IMPORTANT NOTE](#important-note)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```
- NPM/Node.js (LTS – Long Term Support version)
- Visual Studio Code Text Editor/ IntelliJ
- Browser Developer Tools for testing and debugging
    -  React and Redux Dev Tools
- Git
```

## Usage

```
- Clone repository
- Navigate to root folder with terminal
- Execute `npm i` to install all dependencies
- When all dependencies have been downloaded, run by executing `npm start`
```

## API

Template:

https://github.com/dewald-els/noroff-assignment-api

API used in template (created with glitch.com):

https://snapdragon-reliable-badge.glitch.me/

Documentation for "Translation"-endpoints:

https://github.com/dewald-els/noroff-assignment-api/blob/master/docs/lost-in-translation.md

## Maintainers

[@AdamKrason](https://gitlab.com/AdamKrason)

[@eliohk](https://gitlab.com/eliohk)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2023 Adam & Khoi
